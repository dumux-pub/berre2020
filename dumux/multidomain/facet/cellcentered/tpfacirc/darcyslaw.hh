// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \copydoc Dumux::CCTpfaFacetCouplingDarcysLawCirc
 */
#ifndef DUMUX_MULTIDOMAIN_CC_TPFACIRC_FACET_COUPLING_DARCYS_LAW_HH
#define DUMUX_MULTIDOMAIN_CC_TPFACIRC_FACET_COUPLING_DARCYS_LAW_HH

#include <vector>

#include <dune/common/fmatrix.hh>
#include <dune/common/dynmatrix.hh>
#include <dune/common/dynvector.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/cellcentered/tpfacirc/computetransmissibility.hh>
#include <dumux/discretization/cellcentered/tpfacirc/darcyslaw.hh>

namespace Dumux {

//! Forward declaration of the implementation
template<class TypeTag, bool isNetwork>
class CCTpfaFacetCouplingDarcysLawCircImpl;

/*!
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief The cache corresponding to tpfa Darcy's Law with facet coupling
 * \note We distinguish between network and non-network grids here. Specializations
 *       for the two cases can be found below.
 */
template<class TypeTag, bool isNetwork>
class CCTpfaFacetCouplingDarcysLawCircCache;

/*!
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief Darcy's law for cell-centered finite volume schemes with two-point flux approximation
 *        in the context of coupled models where the coupling occurs across the facets of the bulk
 *        domain elements with a lower-dimensional domain living on these facets.
 */
template<class TypeTag>
using CCTpfaFacetCouplingDarcysLawCirc =
      CCTpfaFacetCouplingDarcysLawCircImpl< TypeTag, ( int(GetPropType<TypeTag, Properties::GridView>::dimension)
                                                     < int(GetPropType<TypeTag, Properties::GridView>::dimensionworld) ) >;

/*!
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief Specialization of the FacetCouplingTpfaDarcysLawCache for non-network grids.
 */
template<class TypeTag>
class CCTpfaFacetCouplingDarcysLawCircCache<TypeTag, /*isNetwork*/false>
{
    using AdvectionType = GetPropType<TypeTag, Properties::AdvectionType>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Scalar = typename GridVariables::Scalar;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using Element = typename FVGridGeometry::GridView::template Codim<0>::Entity;

public:
    //! export the corresponding filler class
    using Filler = TpfaDarcysLawCacheFiller<FVGridGeometry>;

    //! we store the transmissibilities associated with the interior
    //! cell, outside cell, and the fracture facet in an array. Access
    //! to this array should be done using the following indices:
    static constexpr int insideTijIdx = 0;
    static constexpr int outsideTijIdx = 1;
    static constexpr int facetTijIdx = 2;

    //! Export transmissibility storage type
    using AdvectionTransmissibilityContainer = std::array<Scalar, 3>;

    //! update subject to a given problem
    template< class Problem >
    void updateAdvection(const Problem& problem,
                         const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const ElementVolumeVariables& elemVolVars,
                         const SubControlVolumeFace &scvf)
    {
        tij_ = AdvectionType::calculateTransmissibility(problem, element, fvGeometry, elemVolVars, scvf);
    }

    //! We use the same name as in the TpfaDarcysLawCache so
    //! that this cache and the law implementation for non-coupled
    //! models can be reused here on facets that do not lie on an
    //! interior boundary, i.e. do not coincide with a fracture
    Scalar advectionTij() const { return tij_[insideTijIdx]; }

    //! returns the transmissibility associated with the inside cell
    Scalar advectionTijInside() const { return tij_[insideTijIdx]; }

    //! returns the transmissibility associated with the outside cell
    Scalar advectionTijOutside() const {return tij_[outsideTijIdx]; }

    //! returns the transmissibility associated with the outside cell
    Scalar advectionTijFacet() const {return tij_[facetTijIdx]; }

private:
    std::array<Scalar, 3> tij_;
};

/*!
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief Specialization of the CCTpfaDarcysLaw grids where dim=dimWorld
 */
template<class TypeTag>
class CCTpfaFacetCouplingDarcysLawCircImpl<TypeTag, /*isNetwork*/false>
{
    using TpfaDarcysLaw = CCTpfaDarcysLawCirc<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Scalar = typename GridVariables::Scalar;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using ElementFluxVarsCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FluxVariablesCache = typename ElementFluxVarsCache::FluxVariablesCache;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using IndexType = typename GridView::IndexSet::IndexType;

    //! Compute the transmissibility associated with the facet element
    template<class FacetVolVars>
    static Scalar computeFacetTransmissibility_(const VolumeVariables& insideVolVars,
                                                const FacetVolVars& facetVolVars,
                                                const SubControlVolumeFace& scvf)
    {
        return 2.0*scvf.area()*insideVolVars.extrusionFactor()
                              /facetVolVars.extrusionFactor()
                              *vtmv(scvf.unitOuterNormal(), facetVolVars.permeability(), scvf.unitOuterNormal());
    }

  public:
    //! export the discretization method this implementation belongs to
    static const DiscretizationMethod discMethod = DiscretizationMethod::cctpfa;
    //! export the type for the corresponding cache
    using Cache = CCTpfaFacetCouplingDarcysLawCircCache<TypeTag, /*isNetwork*/false>;
    //! export the type used to store transmissibilities
    using TijContainer = typename Cache::AdvectionTransmissibilityContainer;

    //! Compute the advective flux
    template< class Problem >
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       int phaseIdx,
                       const ElementFluxVarsCache& elemFluxVarsCache)
    {
        if (!problem.couplingManager().isOnInteriorBoundary(element, scvf))
            return TpfaDarcysLaw::flux(problem, element, fvGeometry, elemVolVars, scvf, phaseIdx, elemFluxVarsCache);

        // Obtain inside and fracture pressures
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& facetVolVars = problem.couplingManager().getLowDimVolVars(element, scvf);
        const auto pInside = insideVolVars.pressure(phaseIdx);
        const auto pFacet = facetVolVars.pressure(phaseIdx);

        // compute and return flux
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];
        Scalar flux = fluxVarsCache.advectionTijInside()*pInside + fluxVarsCache.advectionTijFacet()*pFacet;

        return scvf.boundary() ? flux
                               : flux + fluxVarsCache.advectionTijOutside()*elemVolVars[scvf.outsideScvIdx()].pressure(phaseIdx);
    }

    // The flux variables cache has to be bound to an element prior to flux calculations
    // During the binding, the transmissibility will be computed and stored using the method below.
    template< class Problem >
    static TijContainer calculateTransmissibility(const Problem& problem,
                                                  const Element& element,
                                                  const FVElementGeometry& fvGeometry,
                                                  const ElementVolumeVariables& elemVolVars,
                                                  const SubControlVolumeFace& scvf)
    {
        TijContainer tij;
        if (!problem.couplingManager().isCoupled(element, scvf))
        {
            //! use the standard darcy's law and only compute one transmissibility
            tij[FluxVariablesCache::insideTijIdx] = TpfaDarcysLaw::calculateTransmissibility(problem,
                                                                                             element,
                                                                                             fvGeometry,
                                                                                             elemVolVars,
                                                                                             scvf);
            return tij;
        }

        const auto insideScvIdx = scvf.insideScvIdx();
        const auto& insideScv = fvGeometry.scv(insideScvIdx);
        const auto& insideVolVars = elemVolVars[insideScvIdx];
        const auto wIn = scvf.area()*computeTpfaTransmissibilityCirc(scvf,
                                                                     insideScv,
                                                                     insideVolVars.permeability(),
                                                                     insideVolVars.extrusionFactor());

        // proceed depending on the interior BC types used
        const auto iBcTypes = problem.interiorBoundaryTypes(element, scvf);

        // neumann-coupling
        if (iBcTypes.hasOnlyNeumann())
        {
            const auto outsideScvIdx = scvf.outsideScvIdx();
            const auto& outsideVolVars = elemVolVars[outsideScvIdx];
            const auto wOut = -1.0*scvf.area()*computeTpfaTransmissibilityCirc(scvf,
                                                                               fvGeometry.scv(outsideScvIdx),
                                                                               outsideVolVars.permeability(),
                                                                               outsideVolVars.extrusionFactor());

            const auto& facetVolVars = problem.couplingManager().getLowDimVolVars(element, scvf);
            const auto wFacet = computeFacetTransmissibility_(insideVolVars, facetVolVars, scvf);
            static const Scalar xi = getParam<Scalar>("Problem.FacetCoupling.Xi", 1.0);

            const auto scalar = wIn * wFacet / ( wIn * wOut * ( 2.0 * xi - 1.0 ) + wFacet * ( xi * ( wIn + wOut ) + wFacet ) );

            tij[FluxVariablesCache::insideTijIdx]  = scalar * ( wOut * xi + wFacet );
            tij[FluxVariablesCache::outsideTijIdx] = scalar * ( wOut * ( 1.0 - xi ) );
            tij[FluxVariablesCache::facetTijIdx]   = scalar * ( - wOut - wFacet );
        }
        else if (iBcTypes.hasOnlyDirichlet())
        {
            tij[FluxVariablesCache::insideTijIdx] = wIn;
            tij[FluxVariablesCache::outsideTijIdx] = 0.0;
            tij[FluxVariablesCache::facetTijIdx] = -wIn;
        }
        else
            DUNE_THROW(Dune::NotImplemented, "Interior boundary types other than pure Dirichlet or Neumann");

        return tij;
    }
};

} // end namespace Dumux

#endif
