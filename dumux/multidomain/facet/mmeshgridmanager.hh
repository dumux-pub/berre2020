/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \brief Contains the grid manager class that creates the grids in the context
 *        of hybrid-dimensional coupled models, where the (n-1)-dimensional
 *        domains live on the element facets of the n-dimensional domains.
 *        Also, it allows to extract a grid data object containing parameters
 *        passed to elements and/or boundary segments. All grids are constructed
 *        from a single grid file and can be adapted using this manager.
 */
#ifndef DUMUX_MULTIDOMAIN_FACET_MMESHGRIDMANAGER_HH
#define DUMUX_MULTIDOMAIN_FACET_MMESHGRIDMANAGER_HH

#include <dune/common/hybridutilities.hh>

#include <dumux/multidomain/facet/gridmanager.hh>

namespace Dumux {

/*!
 * \ingroup FacetCoupling
 * \brief Wraps the standard grid manager in the context of hybrid-dimensional coupled models
 *        to be used with MMesh and its InterfaceGrid.
 *
 * \tparam MMesh the type of the mmesh grid
 * \tparam InterfaceGrid the type of the mmesh interface grid
 */
template<typename MMesh>
class FacetCouplingGridManager< MMesh, typename MMesh::InterfaceGrid >
{
    using InterfaceGrid = typename MMesh::InterfaceGrid;
    // make sure all grids have the same world dimension and are ordered in descending dimension
    static_assert(MMesh::dimensionworld == InterfaceGrid::dimensionworld, "All grids must have the same world dimension!");
    static_assert(MMesh::dimension > InterfaceGrid::dimension, "Grids must be ordered w.r.t the dimension in descending order!");

    // we use a wrapper class for the grid data containing the data on all grids
    using GridDataWrapper = FacetCouplingGridDataWrapper<MMesh, InterfaceGrid>;
public:
    //! export the i-th grid type
    template<std::size_t id> using Grid = typename std::tuple_element_t<id, std::tuple<MMesh, InterfaceGrid>>;
    //! export the i-th grid pointer type
    template<std::size_t id> using GridPtr = typename std::shared_ptr< Grid<id> >;

    //! export the number of created grids
    static constexpr std::size_t numGrids = 2;
    //! export the grid id of the bulk grid (descending grid dim -> always zero!)
    static constexpr int bulkGridId = 0;
    //! export the grid id of the frac grid (descending grid dim -> always one!)
    static constexpr std::size_t fracGridId = 1;

    //! export the grid data (wrapper) type, i.e. parameters/markers
    using GridData = GridDataWrapper;
    //! export the type storing the embeddingsPtr_
    using Embeddings = FacetCouplingEmbeddings<MMesh, InterfaceGrid>;

    //! returns the i-th grid
    template<std::size_t id>
    const Grid<id>& grid() const
    { return *std::get<id>(gridPtrTuple_); }

    //! return a pointer to the grid data object
    std::shared_ptr<const GridData> getGridData() const
    {
        if (!enableEntityMarkers_)
            DUNE_THROW(Dune::IOError, "No grid data available");
        return gridDataPtr_;
    }

    //! return a pointer to the object containing embeddingsPtr_
    std::shared_ptr<const Embeddings> getEmbeddings() const
    { return embeddingsPtr_; }

    //! creates the grids from a file given in parameter tree
    void init(const std::string& paramGroup = "")
    {
        // reset the grid & embedding data
        gridDataPtr_ = std::make_shared<GridData>();
        embeddingsPtr_ = std::make_shared<Embeddings>();

        // get filename and determine grid file extension
        const auto fileName = getParamFromGroup<std::string>(paramGroup, "Grid.File");
        const auto ext = this->getFileExtension(fileName);

        // get some parameters
        const bool verbose = getParamFromGroup<bool>(paramGroup, "Grid.Verbosity", false);
        const bool domainMarkers = getParamFromGroup<bool>(paramGroup, "Grid.DomainMarkers", false);
        const bool boundarySegments = getParamFromGroup<bool>(paramGroup, "Grid.BoundarySegments", false);

        // forward to the corresponding reader
        if (ext == "msh")
        {
            const auto thresh = getParamFromGroup<std::size_t>(paramGroup, "Grid.GmshPhysicalEntityThreshold", 0);
            FacetCouplingGmshReader<Grid<bulkGridId>, this->numGrids> gmshReader;
            gmshReader.read(fileName, (boundarySegments ? thresh : 0), verbose);
            passDataFromReader(gmshReader, domainMarkers, boundarySegments);
        }
        else
            DUNE_THROW(Dune::NotImplemented, "Reader for grid files of type ." + ext);

        // find out if entity markers are active
        this->enableEntityMarkers_ = domainMarkers || boundarySegments;
    }

    //! Creates the grids using the data in a mesh file reader
    template<typename MeshFileReader>
    void passDataFromReader(MeshFileReader& reader, bool domainMarkers, bool boundarySegments)
    {
      // Build the bulk grid
      const auto& vertices = reader.gridVertices();

      // do this only for the bulk grid
      static constexpr int id = bulkGridId;
      using GridFactory = Dune::GridFactory<Grid<bulkGridId>>;
      auto factoryPtr = std::make_shared<GridFactory>();

      // insert grid vertices
      std::vector<std::size_t> idxToInsertionIdx( reader.vertexIndices(id).size() );
      std::size_t count = 0;
      for (const auto idx : reader.vertexIndices(id))
      {
          factoryPtr->insertVertex(vertices[idx]);
          idxToInsertionIdx[idx] = count++;
      }

      // insert elements
      for (const auto& e : reader.elementData(id))
          factoryPtr->insertElement(e.gt, e.cornerIndices);

      // insert boundary segments
      if (boundarySegments)
          for (const auto& segment : reader.boundarySegmentData(id))
              factoryPtr->insertBoundarySegment(segment);

      // make grid
      auto gridPtr = std::shared_ptr<Grid<bulkGridId>>(factoryPtr->createGrid());

      // maybe create and set grid data object
      if (domainMarkers || boundarySegments)
      {
          typename GridDataWrapper::template GridData<id> gridData( gridPtr,
                                                                    factoryPtr,
                                                                    std::move(reader.elementMarkerMap(id)),
                                                                    std::move(reader.boundaryMarkerMap(id)) );
          gridDataPtr_->template setGridData<id>( std::move(gridData) );
      }

      // copy the embeddingsPtr_
      embeddingsPtr_->template setData<id>( gridPtr,
                                            factoryPtr,
                                            std::move(reader.embeddedEntityMap(id)),
                                            std::move(reader.adjoinedEntityMap(id)),
                                            std::move(reader.vertexIndices(id)),
                                            vertices.size() );

      // set the grid pointer
      std::get<id>(this->gridPtrTuple_) = gridPtr;


      // Build the interface grid
      static constexpr int idf = fracGridId;
      using GridFactoryF = Dune::GridFactory<Grid<fracGridId>>;
      auto factoryPtrF = std::make_shared<GridFactoryF>( gridPtr );

      // insert grid vertices
      for (const auto idx : reader.vertexIndices(idf))
          factoryPtrF->addVertexHandle( factoryPtr->vertexHandles()[idxToInsertionIdx[idx]] );

      // insert elements
      for (const auto& e : reader.elementData(idf))
          factoryPtrF->insertElement(e.gt, e.cornerIndices);

      // insert boundary segments
      if (boundarySegments)
          for (const auto& segment : reader.boundarySegmentData(idf))
              factoryPtrF->insertBoundarySegment(segment);

      // make grid
      auto gridPtrF = std::shared_ptr<Grid<fracGridId>>(factoryPtrF->getGrid());

      // maybe create and set grid data object
      if (domainMarkers || boundarySegments)
      {
          typename GridDataWrapper::template GridData<idf> gridData( gridPtrF,
                                                                    factoryPtrF,
                                                                    std::move(reader.elementMarkerMap(idf)),
                                                                    std::move(reader.boundaryMarkerMap(idf)) );
          gridDataPtr_->template setGridData<idf>( std::move(gridData) );
      }

      // copy the embeddingsPtr_
      embeddingsPtr_->template setData<idf>( gridPtrF,
                                            factoryPtrF,
                                            std::move(reader.embeddedEntityMap(idf)),
                                            std::move(reader.adjoinedEntityMap(idf)),
                                            std::move(reader.vertexIndices(idf)),
                                            vertices.size() );

      std::get<idf>(this->gridPtrTuple_) = gridPtrF;
    }

    //! Distributes the grid on all processes of a parallel computation
    void loadBalance() {}

private:
    //! return non-const reference to i-th grid
    template<std::size_t id>
    Grid<id>& grid_()
    { return *std::get<id>(gridPtrTuple_); }

    //! Returns the filename extension of a given filename
    static std::string getFileExtension(const std::string& fileName)
    {
        const auto pos = fileName.rfind('.', fileName.length());
        if (pos != std::string::npos)
            return(fileName.substr(pos+1, fileName.length() - pos));
        else
            DUNE_THROW(Dune::IOError, "Please provide an extension for your grid file ('"<< fileName << "')!");
    }

    //! tuple to store the grids
    using Indices = std::make_index_sequence<numGrids>;
    using GridPtrTuple = typename makeFromIndexedType<std::tuple, GridPtr, Indices>::type;
    GridPtrTuple gridPtrTuple_;

    //! grid data, i.e. parameters and markers
    bool enableEntityMarkers_;
    std::shared_ptr<GridData> gridDataPtr_;

    //! data on embeddingsPtr_
    std::shared_ptr<Embeddings> embeddingsPtr_;
};

} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_FACET_MMESHGRIDMANAGER_HH
