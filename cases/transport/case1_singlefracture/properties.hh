// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Includes all properties necessary for an example and defines the
 *        coupling manager property for a test and all possible type tags
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE1_SINGLEFRACTURE_PROPERTIES_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE1_SINGLEFRACTURE_PROPERTIES_HH

#include <dumux/common/properties.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include "problem_1p_matrix.hh"
#include "problem_1p_fracture.hh"

#include "problem_tracer_matrix.hh"
#include "problem_tracer_fracture.hh"

// obtain/define some types to be used below in the property definitions and in main
template<class MatrixTypeTag, class FractureTypeTag>
class TestTraits
{
    using MatrixFVGridGeometry = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::GridGeometry>;
    using FractureFVGridGeometry = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::GridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property in all sub-problems
namespace Dumux {
namespace Properties {

template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePMatrixTpfa> { using type = typename TestTraits<TTag::OnePMatrixTpfa, TTag::OnePFractureTpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePFractureTpfa> { using type = typename TestTraits<TTag::OnePMatrixTpfa, TTag::OnePFractureTpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerMatrixTpfa> { using type = typename TestTraits<TTag::TracerMatrixTpfa, TTag::TracerFractureTpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerFractureTpfa> { using type = typename TestTraits<TTag::TracerMatrixTpfa, TTag::TracerFractureTpfa>::CouplingManager; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePMatrixTpfaCirc> { using type = typename TestTraits<TTag::OnePMatrixTpfaCirc, TTag::OnePFractureTpfaCirc>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePFractureTpfaCirc> { using type = typename TestTraits<TTag::OnePMatrixTpfaCirc, TTag::OnePFractureTpfaCirc>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerMatrixTpfaCirc> { using type = typename TestTraits<TTag::TracerMatrixTpfaCirc, TTag::TracerFractureTpfaCirc>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerFractureTpfaCirc> { using type = typename TestTraits<TTag::TracerMatrixTpfaCirc, TTag::TracerFractureTpfaCirc>::CouplingManager; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePMatrixMpfa> { using type = typename TestTraits<TTag::OnePMatrixMpfa, TTag::OnePFractureMpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePFractureMpfa> { using type = typename TestTraits<TTag::OnePMatrixMpfa, TTag::OnePFractureMpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerMatrixMpfa> { using type = typename TestTraits<TTag::TracerMatrixMpfa, TTag::TracerFractureMpfa>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerFractureMpfa> { using type = typename TestTraits<TTag::TracerMatrixMpfa, TTag::TracerFractureMpfa>::CouplingManager; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePMatrixBox> { using type = typename TestTraits<TTag::OnePMatrixBox, TTag::OnePFractureBox>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::OnePFractureBox> { using type = typename TestTraits<TTag::OnePMatrixBox, TTag::OnePFractureBox>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerMatrixBox> { using type = typename TestTraits<TTag::TracerMatrixBox, TTag::TracerFractureBox>::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, TTag::TracerFractureBox> { using type = typename TestTraits<TTag::TracerMatrixBox, TTag::TracerFractureBox>::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

#endif
