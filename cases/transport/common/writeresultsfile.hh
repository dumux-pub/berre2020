// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Free function to write out the number of cells and number of degrees
 *        of freedom as specified in point 3 of Subsection 5.4 of the document.
 */
#include <iostream>
#include <fstream>

#include <dune/common/indices.hh>
#include <dune/common/hybridutilities.hh>

/*!
 * \brief Free function to write out the number of cells and number of degrees
 *        of freedom as specified in point 3 of Subsection 5.4 of the document.
 * \note If additional output is seeked, an optional lambda can be passed to the
 *       function in which you can specify additional data added to the output file
 *       before a line break.
 * \note Make sure to start your output with a comma and a space before the first value.
 */
template< class FVGridGeometryMatrix, class FVGridGeometryFracture, class Matrix, class AdditionalOutput >
void writeResultsFile(std::ofstream& resultsFile,
                      const FVGridGeometryMatrix& fvGridGeometryMatrix,
                      const FVGridGeometryFracture& fvGridGeometryFracture,
                      const Matrix& J,
                      AdditionalOutput&& additionalOutputFunction)
{
    // we expect a matrix with 4 sub-blocks
    static_assert(Matrix::N() == 2, "Matrix is expected to have two rows");
    static_assert(Matrix::M() == 2, "Matrix is expected to have two columns");

    // count number of non-zeroes
    std::size_t nnz = 0;
    using namespace Dune::Hybrid;
    forEach(integralRange(Dune::index_constant<2>()), [&](auto&& i)
    {
        forEach(integralRange(Dune::index_constant<2>()), [&](auto&& j)
        {
            for (const auto& row : J[i][j])
                for (const auto& col : row)
                    if (col != 0.0)
                        nnz++;
        });
    });

    const auto numCellsMatrix = fvGridGeometryMatrix.gridView().size(0);
    const auto numCellsFracture = fvGridGeometryFracture.gridView().size(0);
    const auto numDofsMatrix = fvGridGeometryMatrix.numDofs();
    const auto numDofsFracture = fvGridGeometryFracture.numDofs();
    resultsFile << /*number 0d cells*/0 << ","
                << /*number of 1d cells*/0 << ","
                << /*number of 2d cells*/numCellsFracture << ","
                << /*number of 3d cells*/numCellsMatrix << ","
                << /*number of dofs*/numDofsMatrix+numDofsFracture << ","
                << /*number of nonzeroes*/nnz;

    // add additional optional output
    additionalOutputFunction(resultsFile);

    // finish with newline character
    resultsFile << std::endl;
}

/*!
 * \brief Convenience overload if no additional output is desired
 */
template< class FVGridGeometryMatrix, class FVGridGeometryFracture, class Matrix >
void writeResultsFile(std::ofstream& resultsFile,
                      const FVGridGeometryMatrix& fvGridGeometryMatrix,
                      const FVGridGeometryFracture& fvGridGeometryFracture,
                      const Matrix& J)
{
    auto emptyAdditionalOutput = [] (auto& file) {};
    writeResultsFile(resultsFile, fvGridGeometryMatrix, fvGridGeometryFracture, J, emptyAdditionalOutput);
}
