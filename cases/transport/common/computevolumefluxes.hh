// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \copydoc computeVolumeFluxes
 */

/*!
 * \brief Free function that computes the volume fluxes on a finite volume
 *        grid and stores one flux per sub-control volume face
 */
template<class FV, class Storage, class CM, class Assembler, class Prob, class GV, class Sol, std::size_t id>
void computeVolumeFluxes(Storage& volumeFluxes,
                         CM& couplingManager,
                         const Assembler& assembler,
                         const Prob& problem,
                         const typename GV::GridGeometry& fvGridGeometry,
                         const GV& gridVariables,
                         const Sol& sol,
                         Dune::index_constant<id> domainId)
{
    static constexpr bool isBox = GV::GridGeometry::discMethod == Dumux::DiscretizationMethod::box;

    // resize depending on the scheme
    if (!isBox) volumeFluxes.assign(fvGridGeometry.numScvf(), {0.0});
    else volumeFluxes.assign(fvGridGeometry.gridView().size(0), {0.0});

    auto upwindTerm = [] (const auto& volVars) { return volVars.mobility(0); };
    for (const auto& element : elements(fvGridGeometry.gridView()))
    {
        const auto eIdx = fvGridGeometry.elementMapper().index(element);

        // bind local views
        couplingManager.bindCouplingContext(domainId, element, assembler);
        auto fvGeometry = localView(fvGridGeometry);
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, sol);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        if (isBox)
            volumeFluxes[eIdx].resize(fvGeometry.numScvf(), 0.0);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            typename GV::Scalar flux = 0.0;
            FV fluxVars;
            fluxVars.init(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            if (!scvf.boundary())
                flux = fluxVars.advectiveFlux(0, upwindTerm);
            else if (!isBox && problem.boundaryTypes(element, scvf).hasOnlyDirichlet())
                flux = fluxVars.advectiveFlux(0, upwindTerm);
            else if (isBox
                     && problem.boundaryTypes(element, fvGeometry.scv(scvf.insideScvIdx())).hasOnlyDirichlet())
            {
                // reconstruct flux
                const auto elemSol = elementSolution(element, sol, fvGridGeometry);
                const auto gradP = evalGradients(element,
                                                 element.geometry(),
                                                 fvGridGeometry,
                                                 elemSol,
                                                 scvf.ipGlobal())[0];
                const auto& insideVolVars = elemVolVars[fvGeometry.scv(scvf.insideScvIdx())];
                const auto k = insideVolVars.permeability();
                flux = (gradP*scvf.unitOuterNormal());
                flux *= -1.0*k;
                flux *= upwindTerm(insideVolVars);
                flux *= scvf.area();
                flux *= insideVolVars.extrusionFactor();
            }
            else if ( (isBox && problem.boundaryTypes(element, fvGeometry.scv(scvf.insideScvIdx())).hasOnlyNeumann())
                     || (!isBox && problem.boundaryTypes(element, scvf).hasOnlyNeumann()) )
            {
                // obtain neumann fluxes (scale with density to get volume flux)
                flux = problem.neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)[0];
                flux *= scvf.area();
                flux /= elemVolVars[scvf.insideScvIdx()].density();
                flux *= elemVolVars[scvf.insideScvIdx()].extrusionFactor();
            }

            if (isBox) volumeFluxes[eIdx][scvf.index()] = flux;
            else volumeFluxes[scvf.index()][0] = flux;
        }
    }
}
