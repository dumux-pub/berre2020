// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial parameters class for the matrix domain in the flow
 *        problem of the benchmark case 2 - regular
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE2_REGULAR_FRACTURE_FLOW_SPATIALPARAMS_MATRIX_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE2_REGULAR_FRACTURE_FLOW_SPATIALPARAMS_MATRIX_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

template< class FVGridGeometry, class Scalar >
class CaseTwoSpatialParamsOnePMatrix
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, CaseTwoSpatialParamsOnePMatrix<FVGridGeometry, Scalar> >
{
    using ThisType = CaseTwoSpatialParamsOnePMatrix< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    CaseTwoSpatialParamsOnePMatrix(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {}

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return isInLowPermRegion(globalPos) ? 0.1 : 1.0; }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 0.1; }

    //! returns true if position is in low permeability region
    bool isInLowPermRegion(const GlobalPosition& globalPos) const
    { return isInOmegaLow1(globalPos) || isInOmegaLow2(globalPos) || isInOmegaLow3(globalPos); }

    bool isInOmegaLow1(const GlobalPosition& globalPos) const
    { return globalPos[0] > 0.5 && globalPos[1] < 0.5; }

    bool isInOmegaLow2(const GlobalPosition& globalPos) const
    { return globalPos[0] > 0.75 && globalPos[1] < 0.75 && globalPos[1] > 0.5 && globalPos[2] > 0.5; }

    bool isInOmegaLow3(const GlobalPosition& globalPos) const
    { return globalPos[0] < 0.75 && globalPos[0] > 0.625 && globalPos[1] < 0.625 && globalPos[1] > 0.5 && globalPos[2] > 0.5 && globalPos[2] < 0.75; }
};

} // end namespace Dumux

#endif
