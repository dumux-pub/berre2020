#!/bin/bash

case $1 in

    case1)
    shift
    /data/berre2020/build-cmake/cases/transport/case1_singlefracture/case1_singlefracture "$@"
    ;;

    case2)
    shift
    /data/berre2020/build-cmake/cases/transport/case2_regular/case2_regular "$@"
    ;;

    case3)
    shift
    /data/berre2020/build-cmake/cases/transport/case3_small/case3_small "$@"
    ;;

    case4)
    shift
    /data/berre2020/build-cmake/cases/transport/case4_field/case4_field "$@"
    ;;

    *)
    echo "The first arument to this script must specify the case to be run, i.e. case1, case2, case3 or case4"
    exit 1

esac
