# One click install script dumux
DUNE_VERSION=2.7

if [ ! -d "berre2020" ]; then
    echo "berre2020 module not found"
    exit 1
fi

# check some prerequistes
for PRGRM in git cmake gcc g++ pkg-config; do
    if ! [ -x "$(command -v $PRGRM)" ]; then
        echo "Error: $PRGRM is not installed." >&2
        exit 1
    fi
done

# check some library prerequistes
for LIBRARY in libumfpack; do
    if ! [ "$(/sbin/ldconfig -p | grep $LIBRARY)" ]; then
        echo "Error: $LIBRARY is not installed." >&2
        exit 1
    fi
done

currentver="$(gcc -dumpversion)"
requiredver="4.9.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" != "$requiredver" ]; then
    echo "gcc greater than or equal to $requiredver is required!" >&2
    exit 1
fi

echo "*********************************************************************************************"
echo "(1/2) Cloning dependency repositories. This may take a while. Make sure to be connected to the internet."
echo "*********************************************************************************************"
# the core modules
for MOD in common geometry grid localfunctions istl; do
    if [ ! -d "dune-$MOD" ]; then
        git clone -b releases/$DUNE_VERSION https://gitlab.dune-project.org/core/dune-$MOD.git
    else
        echo "Skip cloning dune-$MOD because the folder already exists."
    fi
cd dune-$MOD
git checkout releases/$DUNE_VERSION
cd ..
done

# extension modules
for MOD in dune-foamgrid dune-alugrid; do
    if [ ! -d "$MOD" ]; then
        git clone -b releases/$DUNE_VERSION https://gitlab.dune-project.org/extensions/$MOD.git
    else
        echo "Skip cloning $MOD because the folder already exists."
    fi
cd $MOD
git checkout releases/$DUNE_VERSION
cd ..
done

# clone dune-mmesh only if cgal is found in the required version
_CGAL_CHECK_DIR=_benchmark_install_cgal_check
_CGAL_FOUND=false
mkdir $_CGAL_CHECK_DIR
echo "Testing if CGAL is found by cmake"
echo -e "cmake_minimum_required(VERSION 3.18)\nproject(cgal_check)\nfind_package(CGAL 4.12 REQUIRED)" > $_CGAL_CHECK_DIR/CMakeLists.txt
pushd $_CGAL_CHECK_DIR
    if cmake -B build; then
        echo "CGAL found"
        _CGAL_FOUND=true
    else
        echo "CGAL not found"
        _CGAL_FOUND=false
    fi
popd
rm -rf $_CGAL_CHECK_DIR

if [[ $_CGAL_FOUND == true ]]; then
    if [ ! -d "dune-mmesh" ]; then
        git clone https://gitlab.dune-project.org/samuel.burbulla/dune-mmesh.git

        # checkout specific commit number
        cd dune-mmesh
        git checkout 6480c668f974a9273dc40b97c079e1c3ba8f8638
        cd ..
    else
        echo "Skip cloning dune-mmesh because the folder already exists."
    fi
else
    echo "Skip cloning dune-mmesh because CGAL was not found on your system"
fi

# dumux
if [ ! -d "dumux" ]; then
    git clone -b releases/3.0 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

    # checkout specific commit number
    cd dumux
    git checkout 4352c78ea2e1ddaebd893dd08e1f0701363491fb
    cd ..
else
    echo "Skip cloning dumux because the folder already exists."
fi

echo "*********************************************************************************************"
echo "(2/2) Configure dune modules and dumux. Build the dune libaries. This may take several minutes."
echo "*********************************************************************************************"

# run build
./dune-common/bin/dunecontrol --opts=berre2020/docker/docker.opts all

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "Failed to build the dune libaries."
    echo "*********************************************************************************************"
    exit $?
fi

# echo result
echo "*********************************************************************************************"
echo "Succesfully configured and built dune, dumux and the fracture benchmarks."
echo "*********************************************************************************************"
