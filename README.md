Overview
========

This repository contains the source code to run the benchmark cases defined in

>__Berre I., Boon W., Flemisch B., Fumagalli A., Gläser D., Keilegavlen E., Scotti A., Stefansson I., Tatomir A. (2018)__,<br>
>_Call for participation: Verification benchmarks for singlephase flow in three-dimensional fractured porous media._<br>
>Technical report, arXiv:1710.00556 [math.AP]

with the numerical schemes available in _DuMu<sup>x</sup>_. In particular, the results for the schemes "USTUTT\_MPFA" and "USTUTT\_TPFA\_CIRC" as shown in the associated publication

>__Berre I., Wietse B., Flemisch B., Fumagalli A., Gläser D., Keilegavlen E., Scotti A., Stefansson I., Tatomir A.,
Brenner K., Burbulla S., Devloo P., Duran O., Favino M., Hennicker J., Lee I., Lipnikov K., Masson R., Mosthaf K., Giuseppina M., Nestola C., Ni C.,
Nikitin K., Scḧadle P., Svyatsi D., Yanbaris R., Zulian P. (2020)__<br>
> _Verification benchmarks for single-phase flow in three-dimensional fractured porous media_.<br>
> Advances in Water Resources, Volume 147, 2021, 103759, ISSN 0309-1708,
> https://doi.org/10.1016/j.advwatres.2020.103759.

can be reproduced with the code provided in this repository. In addition, the examples can be run with the TPFA and BOX schemes available in Dumux.


Web application
===============

The code provided in this repository can be explored via an interactive web application provided in the
[DaRUS](https://darus.uni-stuttgart.de/) dataset available at [doi.org/10.18419/darus-3228](https://doi.org/10.18419/darus-3228).
All four benchmark cases presented in the above-mentioned references are accessible via the web application,
while allowing you to change a few selected parameters.


Dockerfile and Docker image
===========================

If you do not want to install this software and its dependencies on your local machine (see instructions further
below), you may also explore it from within a `docker` container. A Dockerfile and a prebuilt docker image are 
contained in the dataset at [doi.org/10.18419/darus-3228](https://doi.org/10.18419/darus-3228). After downloading it,
the prebuilt image can directly be loaded into your local container registry with `docker load berre2020_docker_image.tar.gz`.
In order to build the image locally, download the `Dockerfile` and source code (`berre2020.tar.gz`) into a new directory,
extract the source code and type `docker build -t berre2020 .`. This may take some time as it installs all dependencies 
into the container image and compiles the application.

For exploration of the code inside the container, spin up an interactive session via
`docker run -it --entrypoint /bin/bash berre2020`. Afterwards, you can head to the directory with
the application code with `cd /data/berre2020`. For instructions on how to execute the individual benchmark
cases, see the `Execution` section further below. In order to copy simulation results from the container
to your host machine, see the [docker cp](https://docs.docker.com/engine/reference/commandline/cp/) command.


Installation
============

For building from source, you may execute the installation script `install.sh` from one level above:

```bash
chmod u+x install.sh
cd ..
./berre2020/install.sh
```

This will download, configure and compile all dune dependencies.
Furthermore, you need to have the following basic requirement installed

* CMake 2.8.12
* C, C++ compiler (C++14 required)
* Fortran compiler (gfortran)
* ParaView (pvpython)
* UMFPack from SuiteSparse
* CGAL (only required for scheme "USTUTT\_TPFA\_CIRC")

On Ubuntu, all of the above packages can be installed via the package manager. For __UMFPACK__ and __CGAL__,
you may use the following commands:

```bash
sudo apt-get install libsuitesparse-dev
sudo apt-get install libcgal-dev
```

Execution
=========
In the directory
__berre2020/build-cmake/cases/transport__
you will find a directory for each case
```bash
transport/
├ case1_singlefracture
├ case2_regular
├ case3_small
└ case4_field
```

Within each specific case directory, you can compile the corresponding benchmark with

```bash
make CASE_NAME
```

where `CASE_NAME` is identical to the name of the case directory. The simulation can be
triggered with

```bash
./CASE_NAME params.input -Discretization.Scheme SCHEME
```

where `SCHEME` can be chosen between `box`, `tpfa`, `mpfa`, `tpfacirc`. Alternatively,
you can use the convenience scripts `runscheme.py` available for each case, which run
the simulations and produce all secondary data, i.e. plots over lines as requested in
`Berre et al. (2018)`:

```bash
# you may again choose tpfa, mpfa, tpfacirc, box
python3 runscheme.py tpfa
```

Note that the script for `case2_regular` requires you to also pass the index of the
conductivity parameter (`0` for highly-conductive fractures and `1` for blocking fractures)
as defined in `Berre et al. (2018)`.

Note also that the scripts `runscheme.py` perform the simulations and produce plot data for
all refinement levels as specified in `Berre et al. (2018)`. Moreover, for the generation of the
plot data __pvpython__ is required. In order to run a single refinement, you can trigger the
simulation and invoke the plot script manually. For instance, for `case1_singlefracture`:

```bash
cd case1_singlefracture

# the grid with ~1k cells corresponds to refinement 0 in Berre et al (2018)
./case1_singlefracture params.input -Grid.File grids/single_1k.msh -Discretization.Scheme tpfa

# invoke the plot script to produce the plot-over-line data for refinement 0
# the script produces the files `pol_1.csv`, `pol_2.csv`, `pol_3.csv` as requested in Berre et al. (2018)
pvpython makeplotoverlinedata.py tpfa 0
```

License
=========

This module is licensed under the terms and conditions of the GNU General
Public License (GPL) version 3 or - at your option - any later
version. The GPL can be [read online](https://www.gnu.org/licenses/gpl-3.0.en.html) or in the [LICENSE](LICENSE) file
provided in the topmost directory of the module source code tree.
